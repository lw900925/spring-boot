INSERT INTO `sb_user` VALUES ('root',1,1,1,1,1,'$2a$10$yurclUXK8yPBMXq48R/JOO/gPyGSxt9nPntf9uzHDKHnmz4RbQSDu'),('user',1,1,2,1,1,'$2a$10$FCGoiDits7Dk.mBP8ETUseHohoD3mvH08shEXMuM8TXksPv3PQ0cO');
INSERT INTO `sb_role` VALUES ('ADMIN', 'Role for administration'),('USER', 'Role for normal user');
INSERT INTO `sb_user_role_ref` VALUES ('root','ADMIN'),('root','USER'),('user','USER');
INSERT INTO `sb_client` VALUES ('94984796',NULL,'{"field":"productId","value":"1","name":"味全乳酸菌（草莓味）","label":"产品"}','$2a$10$9CW3Kzt4bDDIVmoGM6nRu.tbQKOGs81lzMJ9kx.Moe8qOUkAJjMye',NULL);
INSERT INTO `sb_client_grant_types` VALUES ('94984796','authorization_code'),('94984796','password');
INSERT INTO `sb_client_scopes` VALUES ('94984796','app');
INSERT INTO `sb_enterprise` VALUES (1,'2018-06-08 16:59:40.951','root',1,'中国-上海','矩阵工作室2017年成立于上海市，是一家专注提供Java技能培训的团队','矩阵工作室','025-12345678');