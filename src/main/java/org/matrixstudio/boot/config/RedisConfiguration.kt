package org.matrixstudio.boot.config

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.SpringBootConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer
import org.springframework.data.redis.serializer.GenericToStringSerializer
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer

@SpringBootConfiguration
class RedisConfiguration {

    @Bean
    fun redisTemplate(factory: RedisConnectionFactory, stringSerializer: GenericToStringSerializer<Any>,
                      jsonRedisSerializer: GenericJackson2JsonRedisSerializer): RedisTemplate<String, Any> {
        return RedisTemplate<String, Any>().apply {
            setConnectionFactory(factory)
            keySerializer = stringSerializer
            valueSerializer = jsonRedisSerializer
            hashKeySerializer = stringSerializer
            hashValueSerializer = jsonRedisSerializer
        }
    }

    @Bean
    fun genericToStringSerializer(): GenericToStringSerializer<Any> {
        return GenericToStringSerializer(Any::class.java)
    }

    @Bean
    fun genericJackson2JsonRedisSerializer(objectMapper: ObjectMapper): GenericJackson2JsonRedisSerializer {
        val redisObjectMapper = objectMapper.copy().apply {
            setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY)
            enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL)
        }
        return GenericJackson2JsonRedisSerializer(redisObjectMapper)
    }

    @Bean
    fun jdkSerializationRedisSerializer(): JdkSerializationRedisSerializer {
        return JdkSerializationRedisSerializer()
    }
}