package org.matrixstudio.boot.config

import org.matrixstudio.boot.config.security.CorsFilter
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.data.web.config.EnableSpringDataWebSupport
import org.springframework.hateoas.config.EnableHypermediaSupport
import org.springframework.web.client.RestTemplate
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import javax.servlet.Filter

@SpringBootConfiguration
@EnableSpringDataWebSupport
@EnableHypermediaSupport(type = [EnableHypermediaSupport.HypermediaType.HAL])
class WebMvcConfiguration : WebMvcConfigurer {

    @Bean
    fun restTemplate(restTemplateBuilder: RestTemplateBuilder): RestTemplate {
        return restTemplateBuilder.build()
    }

    @Bean
    fun corsFilter(): Filter {
        return CorsFilter()
    }
}