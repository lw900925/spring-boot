package org.matrixstudio.boot.config.security.oauth2

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
class UserInfoEndpoint {

    @GetMapping("/oauth/user_info")
    fun getPrincipal(principal: Principal): Principal {
        return principal
    }
}