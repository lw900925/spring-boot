package org.matrixstudio.boot.config.security

import org.matrixstudio.boot.repository.jpa.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService

class JpaUserDetailsService(var userRepository: UserRepository) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        return userRepository.findById(username).orElseGet {
            userRepository.findByEmail(username)
        }
    }
}