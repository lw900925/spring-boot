package org.matrixstudio.boot.config.security.oauth2

import org.matrixstudio.boot.repository.jpa.ClientRepository
import org.springframework.security.oauth2.provider.ClientDetails
import org.springframework.security.oauth2.provider.ClientDetailsService

class JpaClientDetailsService(var clientRepository: ClientRepository) : ClientDetailsService {

    override fun loadClientByClientId(clientId: String): ClientDetails {
        return clientRepository.findById(clientId).orElse(null)
    }

}