package org.matrixstudio.boot.config.security.oauth2

import org.springframework.boot.SpringBootConfiguration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter

@SpringBootConfiguration
@EnableResourceServer
class ResourceServerConfiguration : ResourceServerConfigurerAdapter() {

    private val ignored = arrayOf(
            "/css/**",
            "/js/**",
            "/images/**",
            "/webjars/**",
            "/**/favicon.ico",

            "/h2-console/**",

            "/echo")

    override fun configure(http: HttpSecurity) {
        http.csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .authorizeRequests()
                .antMatchers(*ignored).permitAll()
                .anyRequest().access("#oauth2.hasScope('app')")
    }
}