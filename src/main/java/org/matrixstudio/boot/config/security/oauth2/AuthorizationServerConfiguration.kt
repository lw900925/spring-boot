package org.matrixstudio.boot.config.security.oauth2

import org.matrixstudio.boot.repository.jpa.ClientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringBootConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ClassPathResource
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory

@SpringBootConfiguration
@EnableAuthorizationServer
class AuthorizationServerConfiguration(authenticationConfiguration: AuthenticationConfiguration) : AuthorizationServerConfigurerAdapter() {

    @Autowired
    private lateinit var clientRepository: ClientRepository
    var authenticationManager: AuthenticationManager = authenticationConfiguration.authenticationManager

    override fun configure(security: AuthorizationServerSecurityConfigurer) {
        security.allowFormAuthenticationForClients()
    }

    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.withClientDetails(clientDetailsService())
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        // 注：这里需要设置AuthenticationManager对象，否则没有password授权模式
        endpoints.authenticationManager(authenticationManager)
        endpoints.accessTokenConverter(tokenConverter())
    }

    @Bean
    fun tokenConverter(): JwtAccessTokenConverter {
        return JwtAccessTokenConverter().apply {
            val keyPair = KeyStoreKeyFactory(ClassPathResource("keystore.jks"), "123456".toCharArray()).getKeyPair("spring-oauth2")
            setKeyPair(keyPair)
        }
    }

    @Bean
    fun clientDetailsService(): ClientDetailsService {
        return JpaClientDetailsService(clientRepository)
    }
}