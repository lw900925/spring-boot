package org.matrixstudio.boot.config

import org.springframework.boot.SpringBootConfiguration
import org.springframework.cache.annotation.EnableCaching

@SpringBootConfiguration
@EnableCaching
class CacheConfiguration