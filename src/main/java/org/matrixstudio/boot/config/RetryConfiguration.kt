package org.matrixstudio.boot.config

import org.springframework.boot.SpringBootConfiguration
import org.springframework.retry.annotation.EnableRetry

@SpringBootConfiguration
@EnableRetry
class RetryConfiguration