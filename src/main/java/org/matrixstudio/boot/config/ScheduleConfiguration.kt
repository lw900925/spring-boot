package org.matrixstudio.boot.config

import org.springframework.boot.SpringBootConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.TaskScheduler
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler
import java.util.concurrent.Executors

@SpringBootConfiguration
@EnableScheduling
class ScheduleConfiguration {

    /**
     * 这是一个Bug。
     * 当同时使用`@EnableScheduling`和`@EnableWebSocket`注解时候，会抛出异常`java.lang.IllegalStateException: Unexpected use of scheduler`。
     * 希望Spring能在后续版本中修复该问题，GitHub issues：https://github.com/spring-projects/spring-boot/issues/12814
     */
    @Bean
    fun taskScheduler(): TaskScheduler {
        return ConcurrentTaskScheduler(Executors.newScheduledThreadPool(10))
    }
}