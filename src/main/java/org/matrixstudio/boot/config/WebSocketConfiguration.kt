package org.matrixstudio.boot.config

import org.springframework.boot.SpringBootConfiguration
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry
import org.springframework.web.socket.handler.TextWebSocketHandler

@SpringBootConfiguration
@EnableWebSocket
class WebSocketConfiguration : WebSocketConfigurer {

    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(WebSocketHandler(), "/echo").setAllowedOrigins("*")
    }

    class WebSocketHandler : TextWebSocketHandler() {

        override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
            session.sendMessage(TextMessage("这是你刚刚发送的消息：" + message.payload))
        }
    }
}