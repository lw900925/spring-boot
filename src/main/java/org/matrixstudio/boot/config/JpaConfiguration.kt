package org.matrixstudio.boot.config

import org.matrixstudio.boot.support.spring.data.jpa.auditing.AuditorProvider
import org.matrixstudio.boot.support.spring.data.jpa.auditing.LocalDateTimeProvider
import org.springframework.boot.SpringBootConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootConfiguration
@EnableJpaRepositories("org.matrixstudio.boot.repository.jpa")
@EnableJpaAuditing(auditorAwareRef = "auditorProvider", dateTimeProviderRef = "localDateTimeProvider")
class JpaConfiguration {

    @Bean
    fun auditorProvider(): AuditorAware<String> {
        return AuditorProvider()
    }

    @Bean
    fun localDateTimeProvider(): LocalDateTimeProvider {
        return LocalDateTimeProvider()
    }
}