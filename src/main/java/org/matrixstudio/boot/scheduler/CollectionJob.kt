package org.matrixstudio.boot.scheduler

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.scheduling.quartz.SchedulerFactoryBean
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class CollectionJob {

    @Autowired
    lateinit var schedulerFactoryBean: SchedulerFactoryBean

    @Scheduled(cron = "*/5 * * * * ?")
    fun execute() {
        // 每5秒执行一次
        println("当前时间：" + LocalDateTime.now())
    }
}