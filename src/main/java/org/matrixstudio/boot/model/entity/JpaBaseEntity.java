package org.matrixstudio.boot.model.entity;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class JpaBaseEntity implements java.io.Serializable {

    @Id
    @TableGenerator(name = "ID_GENERATOR", initialValue = 5)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ID_GENERATOR")
    private Long oid;
    @CreatedDate
    private LocalDateTime creationDate;
    @CreatedBy
    private String creator;
    @Version
    private Long version = 1L;

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
