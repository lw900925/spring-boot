package org.matrixstudio.boot.model.entity

import org.hibernate.validator.constraints.Length
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "sb_enterprise")
data class Enterprise(
        @get: NotBlank var name: String,
        var address: String?,
        var telephone: String?,
        @get: Length(max = 255) var intro: String?): JpaBaseEntity()