package org.matrixstudio.boot.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.matrixstudio.boot.support.spring.data.jpa.usertype.EnumerationType;
import org.matrixstudio.boot.support.spring.data.jpa.usertype.JpaEnumAttribute;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sb_user")
@TypeDef(name = "enumerationType", typeClass = EnumerationType.class)
public class User implements UserDetails {

    private String password;
    @Id
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "assigned")
    private String username;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "sb_user_role_ref",
            joinColumns = { @JoinColumn(name = "username", nullable = false) },
            inverseJoinColumns = { @JoinColumn(name = "authority", nullable = false) })
    private Set<Role> roles = new HashSet<>();

    // ~ Additional fields
    // =================================================================================================================

    private String email;
    private String phoneNumber;
    @Type(type = "enumerationType")
    private ClientCapability clientCapability;
    private String displayName;
    private String avatarUrl;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    // ~ Additional fields getter & setter
    // =================================================================================================================

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ClientCapability getClientCapability() {
        return clientCapability;
    }

    public void setClientCapability(ClientCapability clientCapability) {
        this.clientCapability = clientCapability;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    /**
     * 自定义枚举类字段
     */
    public enum ClientCapability implements JpaEnumAttribute {
        CUSTOMER(1),
        AUDITOR(2),
        SYS_ADMINISTRATOR(3);

        private Integer value;

        ClientCapability(Integer value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public Integer getValue() {
            return value;
        }
    }
}
