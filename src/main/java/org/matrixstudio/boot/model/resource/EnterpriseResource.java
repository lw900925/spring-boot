package org.matrixstudio.boot.model.resource;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;

@Data
@EqualsAndHashCode(callSuper = true)
public class EnterpriseResource extends ResourceSupport {
    private Long oid;
    private String name;
    private String address;
    private String telephone;
    private String intro;
}
