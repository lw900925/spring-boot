package org.matrixstudio.boot.model.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.matrixstudio.boot.model.entity.Role;
import org.springframework.hateoas.ResourceSupport;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper = true)
public class ClientResource extends ResourceSupport {

    @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("client_secret")
    private String clientSecret;

    private Set<String> scope = new HashSet<>();

    @JsonProperty("resource_ids")
    private Set<String> resourceIds = new HashSet<>();

    @JsonProperty("authorized_grant_types")
    private Set<String> authorizedGrantTypes = new HashSet<>();

    @JsonProperty("redirect_uri")
    private Set<String> registeredRedirectUris = new HashSet<>();

    @JsonProperty("autoapprove")
    private Set<String> autoApproveScopes = new HashSet<>();

    private List<Role> authorities = new ArrayList<>();

    @JsonProperty("access_token_validity")
    private Integer accessTokenValiditySeconds;

    @JsonProperty("refresh_token_validity")
    private Integer refreshTokenValiditySeconds;

    private Map<String, Object> additionalInformation = new LinkedHashMap<String, Object>();
}
