package org.matrixstudio.boot.model.resource;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoleResource extends ResourceSupport {
    private String authority;
    private String description;
}
