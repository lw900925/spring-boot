package org.matrixstudio.boot.model.resource;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.matrixstudio.boot.model.entity.Role;
import org.matrixstudio.boot.model.entity.User;
import org.springframework.hateoas.ResourceSupport;

import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserResource extends ResourceSupport {
    private String username;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
    private Set<Role> roles = new HashSet<>();
    private User.ClientCapability clientCapability;
}
