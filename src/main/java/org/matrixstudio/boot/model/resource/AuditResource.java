package org.matrixstudio.boot.model.resource;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.matrixstudio.boot.model.document.Action;
import org.springframework.hateoas.ResourceSupport;

@Data
@EqualsAndHashCode(callSuper = true)
public class AuditResource extends ResourceSupport {
    private String oid;
    private Long date;
    private Action action;
    private String objectId;
}
