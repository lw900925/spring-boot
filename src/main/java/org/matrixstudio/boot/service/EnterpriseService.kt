package org.matrixstudio.boot.service

import org.matrixstudio.boot.model.entity.Enterprise
import org.matrixstudio.boot.repository.jpa.EnterpriseRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityNotFoundException

@Service
@Transactional
class EnterpriseService {

    @Autowired
    private lateinit var enterpriseRepository: EnterpriseRepository

    @Cacheable(value = ["query"], key = "#root.targetClass.name + '-' + #root.methodName")
    @Transactional(readOnly = true)
    fun list(pageable: Pageable, search: String?): Page<Enterprise> {
        return enterpriseRepository.findAll(pageable)
    }

    @Transactional(readOnly = true)
    fun get(oid: Long): Enterprise {
        return enterpriseRepository.findById(oid).orElseThrow({ EntityNotFoundException() })
    }

    @CacheEvict(value = ["query"], allEntries = true)
    fun save(enterprise: Enterprise): Enterprise {
        return enterpriseRepository.save(enterprise)
    }

    @CacheEvict(value = ["query"], allEntries = true)
    fun delete(oid: Long): Enterprise {
        val enterprise = enterpriseRepository.findById(oid).orElse(null)
        enterpriseRepository.delete(enterprise)
        return enterprise
    }
}