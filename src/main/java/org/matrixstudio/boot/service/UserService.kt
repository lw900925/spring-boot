package org.matrixstudio.boot.service

import org.matrixstudio.boot.model.entity.User
import org.matrixstudio.boot.repository.jpa.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.StringUtils

@Service
@Transactional
class UserService {

    @Autowired
    private lateinit var userRepository: UserRepository

    @Transactional(readOnly = true)
    fun list(pageable: Pageable, search: String?): Page<User> {
        return if (StringUtils.hasText(search)) {
            userRepository.findAllByUsernameStartingWith(search, pageable)
        } else {
            userRepository.findAll(pageable)
        }
    }
}