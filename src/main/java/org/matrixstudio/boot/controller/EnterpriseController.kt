package org.matrixstudio.boot.controller

import org.matrixstudio.boot.model.entity.Enterprise
import org.matrixstudio.boot.model.resource.EnterpriseResource
import org.matrixstudio.boot.service.EnterpriseService
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.ExposesResourceFor
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/enterprises")
@ExposesResourceFor(EnterpriseResource::class)
class EnterpriseController {

    private val enterpriseAssembler = Assembler()

    @Autowired
    private lateinit var enterpriseService: EnterpriseService

    @GetMapping
    fun list(pageable: Pageable,
             @RequestParam(required = false) search: String?,
             assembler: PagedResourcesAssembler<Enterprise>): PagedResources<EnterpriseResource> {
        return assembler.toResource(enterpriseService.list(pageable, search), enterpriseAssembler)
    }

    @GetMapping("/{enterpriseId}")
    fun get(@PathVariable enterpriseId: Long): EnterpriseResource {
        return enterpriseAssembler.toResource(enterpriseService.get(enterpriseId))
    }

    @PostMapping
    fun save(@RequestBody @Valid enterprise: Enterprise): EnterpriseResource {
        return enterpriseAssembler.toResource(enterpriseService.save(enterprise))
    }

    @DeleteMapping("/{enterpriseId}")
    fun delete(@PathVariable enterpriseId: Long): EnterpriseResource {
        return enterpriseAssembler.toResource(enterpriseService.delete(enterpriseId))
    }

    internal inner class Assembler : ResourceAssemblerSupport<Enterprise, EnterpriseResource>(EnterpriseController::class.java, EnterpriseResource::class.java) {
        override fun toResource(entity: Enterprise): EnterpriseResource {
            return createResourceWithId(entity.oid, entity)
        }

        override fun instantiateResource(entity: Enterprise): EnterpriseResource {
            val resource = EnterpriseResource()
            BeanUtils.copyProperties(entity, resource)
            return resource
        }
    }
}