package org.matrixstudio.boot.controller;

import org.matrixstudio.boot.model.entity.Role;
import org.matrixstudio.boot.model.entity.User;
import org.matrixstudio.boot.model.resource.UserResource;
import org.matrixstudio.boot.repository.jpa.UserRepository;
import org.matrixstudio.boot.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
@ExposesResourceFor(UserResource.class)
public class UserController {

    private final Assembler userAssembler = new Assembler();

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @GetMapping
    public PagedResources<UserResource> list(Pageable pageable,
                                             @RequestParam(required = false) String search,
                                             PagedResourcesAssembler<User> assembler) {
        return assembler.toResource(userService.list(pageable, search), userAssembler);
    }

    @GetMapping("/{username}")
    public UserResource get(@PathVariable String username) {
        User user = userRepository.findById(username).orElse(null);
        return userAssembler.toResource(user);
    }

    @PostMapping
    public UserResource save(@RequestBody @Valid User user) {
        return userAssembler.toResource(userRepository.save(user));
    }

    class Assembler extends ResourceAssemblerSupport<User, UserResource> {
        public Assembler() {
            super(UserController.class, UserResource.class);
        }

        @Override
        public UserResource toResource(User entity) {
            return createResourceWithId(entity.getUsername(), entity);
        }

        @Override
        protected UserResource instantiateResource(User entity) {
            UserResource resource = new UserResource();
            BeanUtils.copyProperties(entity, resource);

            Set<Role> roles = entity.getAuthorities().stream()
                    .map(authority -> (Role) authority)
                    .collect(Collectors.toSet());
            resource.setRoles(roles);
            return resource;
        }
    }
}
