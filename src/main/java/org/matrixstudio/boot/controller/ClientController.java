package org.matrixstudio.boot.controller;

import org.matrixstudio.boot.model.entity.Client;
import org.matrixstudio.boot.model.resource.ClientResource;
import org.matrixstudio.boot.repository.jpa.ClientRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;

@RestController
@RequestMapping("/clients")
@ExposesResourceFor(ClientResource.class)
public class ClientController {

    private final Assembler clientAssembler = new Assembler();

    @Autowired
    private ClientRepository clientRepository;

    @GetMapping("/{clientId}")
    public ClientResource get(@PathVariable String clientId) {
        Client client = clientRepository.findById(clientId).orElseThrow(EntityNotFoundException::new);
        return clientAssembler.toResource(client);
    }

    class Assembler extends ResourceAssemblerSupport<Client, ClientResource> {
        public Assembler() {
            super(ClientController.class, ClientResource.class);
        }

        @Override
        public ClientResource toResource(Client client) {
            return client != null ? createResourceWithId(client.getClientId(), client) : null;
        }

        @Override
        protected ClientResource instantiateResource(Client entity) {
            ClientResource resource = new ClientResource();
            BeanUtils.copyProperties(entity, resource);
            return resource;
        }
    }
}
