package org.matrixstudio.boot.controller

import org.matrixstudio.boot.model.entity.Role
import org.matrixstudio.boot.model.resource.RoleResource
import org.matrixstudio.boot.repository.jpa.RoleRepository
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.ExposesResourceFor
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/roles")
@ExposesResourceFor(RoleResource::class)
class RoleController {

    private val roleAssembler = Assembler()

    @Autowired
    private lateinit var roleRepository: RoleRepository

    @GetMapping("/{authority}")
    fun get(@PathVariable authority: String): RoleResource {
        val role: Role = roleRepository.findById(authority).orElse(null)
        return roleAssembler.toResource(role)
    }

    internal inner class Assembler : ResourceAssemblerSupport<Role, RoleResource>(RoleController::class.java, RoleResource::class.java) {
        override fun toResource(entity: Role): RoleResource {
            return createResourceWithId(entity.authority, entity)
        }

        override fun instantiateResource(entity: Role): RoleResource {
            val resource = RoleResource()
            BeanUtils.copyProperties(entity, resource)
            return resource
        }
    }
}